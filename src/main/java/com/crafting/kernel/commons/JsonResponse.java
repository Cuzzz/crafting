package com.crafting.kernel.commons;

public class JsonResponse
{

    private boolean success;

    private String errorMessage;

    private String message;

    private int status;

    private Object body;

    public JsonResponse( boolean success )
    {
        this( success, null );
    }

    public JsonResponse( boolean success, int httpStatusCode )
    {
        this( success, null );
        this.status = httpStatusCode;
    }

    public JsonResponse( boolean success, String errorMessage )
    {
        this.success = success;
        this.errorMessage = errorMessage;
    }

    public boolean isSuccess()
    {
        return success;
    }

    public String getErrorMessage()
    {
        return errorMessage;
    }

    public void setSuccess( boolean success )
    {
        this.success = success;
    }

    public void setErrorMessage( String errorMessage )
    {
        this.errorMessage = errorMessage;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage( String message )
    {
        this.message = message;
    }

    public int getStatus()
    {
        return status;
    }

    public void setStatus( int status )
    {
        this.status = status;
    }

    public Object getBody()
    {
        return this.body;
    }

    public JsonResponse setBody( Object body )
    {
        this.body = body;
        return this;
    }

    public JsonResponse errorMessage( String errorMessage )
    {
        setErrorMessage( errorMessage );
        return this;
    }

    public JsonResponse body( Object body )
    {
        setBody( body );
        return this;
    }

    public static synchronized JsonResponse success()
    {
        return new JsonResponse( true );
    }

    public static synchronized JsonResponse failure()
    {
        return new JsonResponse( false );
    }

    public static JsonResponse failure( int httpStatusCode )
    {
        return new JsonResponse( false, httpStatusCode );
    }
}
