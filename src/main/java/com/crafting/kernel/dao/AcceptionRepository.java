package com.crafting.kernel.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.crafting.kernel.domains.Acception;
import com.crafting.kernel.domains.Job;
import com.crafting.kernel.domains.Person;

public interface AcceptionRepository extends JpaRepository<Acception, Long> {
	
	public List<Acception> findByAuthor(Person author);

	public List<Acception> findByJob(Job job);
	
}
