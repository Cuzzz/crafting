package com.crafting.kernel.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.crafting.kernel.domains.Deal;
import com.crafting.kernel.domains.Job;
import com.crafting.kernel.domains.Person;

public interface DealRepository extends JpaRepository<Deal, Long>{

	public List<Deal> findByEmployee(Person employee);
	public List<Deal> findByCustomer(Person customer);
	public List<Deal> findByJob(Job job);
	
}
