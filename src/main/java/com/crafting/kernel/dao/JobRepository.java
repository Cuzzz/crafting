package com.crafting.kernel.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import com.crafting.kernel.domains.Job;
import com.crafting.kernel.domains.Person;

public interface JobRepository extends JpaRepository<Job, Long>{

	public List<Job> findByAuthor(Person author);
	
	public Page<Job> findByStatus(int status, Pageable pageable);

	public List<Job> findByStatus(int code, Sort sort);
	
}
