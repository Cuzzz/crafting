package com.crafting.kernel.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.crafting.kernel.domains.Message;
import com.crafting.kernel.domains.Person;

public interface MessageRepository extends JpaRepository<Message, Long> {

	List<Message> findByAuthorAndDeletedForAuthor(Person person, boolean deletedForAuthor);
	List<Message> findByDestinationAndDeletedForDestination(Person person, boolean deletedForDestination);
	
}
