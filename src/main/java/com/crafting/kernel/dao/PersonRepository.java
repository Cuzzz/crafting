package com.crafting.kernel.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.crafting.kernel.domains.Person;
import com.crafting.kernel.security.User;

public interface PersonRepository extends JpaRepository<Person, Long>{

	public Person findByUser(User user);
	
}
