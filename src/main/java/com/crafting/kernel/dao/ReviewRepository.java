package com.crafting.kernel.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.crafting.kernel.domains.Job;
import com.crafting.kernel.domains.Person;
import com.crafting.kernel.domains.Review;

public interface ReviewRepository extends JpaRepository<Review, Long>{

	public List<Review> findBySubject(Person subject);
	public List<Review> findByAuthor(Person author);
	public List<Review> findByJob(Job job);
	
}
