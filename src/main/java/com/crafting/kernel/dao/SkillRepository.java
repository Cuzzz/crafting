package com.crafting.kernel.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.crafting.kernel.domains.Skill;

public interface SkillRepository extends JpaRepository<Skill, Long>{

	List<Skill> findByTitleStartingWith(String title);
	Skill findByTitle(String title);
	
}
