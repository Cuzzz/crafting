package com.crafting.kernel.domain.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.crafting.kernel.dao.JobRepository;
import com.crafting.kernel.dao.PersonRepository;
import com.crafting.kernel.domains.Acception;
import com.crafting.kernel.dto.AcceptionDto;

@Component
@Scope("singleton")
public class AcceptionMapper {

	@Autowired
	private JobRepository jobRepo;
	
	@Autowired
	private PersonRepository personRepo;
	
	public Acception getAcception(AcceptionDto acceptionDto)
	{
		Acception acception = new Acception();
		acception.setId(acceptionDto.getId());
		acception.setFd(acceptionDto.getFd());
		acception.setTd(acceptionDto.getTd());
		acception.setStatus(acceptionDto.getStatus());
		acception.setBody(acceptionDto.getBody());
		acception.setJob(jobRepo.findOne(acceptionDto.getJobId()));
		acception.setAuthor(personRepo.findOne(acceptionDto.getAuthorId()));
		return acception;
	}
	
	public AcceptionDto getAcceptionDto(Acception acception)
	{
		AcceptionDto dto = new AcceptionDto();
		dto.setId(acception.getId());
		dto.setFd(acception.getFd());
		dto.setTd(acception.getTd());
		dto.setBody(acception.getBody());
		dto.setStatus(acception.getStatus());
		dto.setAuthorId(acception.getAuthor().getId());
		dto.setJobId(acception.getJob().getId());
		return dto;
	}
	
}
