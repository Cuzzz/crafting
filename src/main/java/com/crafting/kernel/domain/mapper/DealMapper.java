package com.crafting.kernel.domain.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.crafting.kernel.dao.JobRepository;
import com.crafting.kernel.dao.PersonRepository;
import com.crafting.kernel.domains.Deal;
import com.crafting.kernel.dto.DealDto;

@Component
@Scope("singleton")
public class DealMapper {

	@Autowired
	private PersonRepository personRepo;
	
	@Autowired
	private JobRepository jobRepo;
	
	public Deal getDeal(DealDto dto)
	{
		Deal deal = new Deal();
		deal.setId(dto.getId());
		deal.setFd(dto.getFd());
		deal.setTd(dto.getTd());
		deal.setStatus(dto.getStatus());
		deal.setJob(jobRepo.getOne(dto.getJobId()));
		deal.setCustomer(personRepo.getOne(dto.getCustomerId()));
		deal.setEmployee(personRepo.getOne(dto.getEmployeeId()));
		return deal;
	}
	
	public DealDto getDealDto(Deal deal)
	{
		DealDto dealDto = new DealDto();
		dealDto.setId(deal.getId());
		dealDto.setFd(deal.getFd());
		dealDto.setTd(deal.getTd());
		dealDto.setJobId(deal.getJob().getId());
		dealDto.setCustomerId(deal.getCustomer().getId());
		dealDto.setEmployeeId(deal.getEmployee().getId());
		return dealDto;
	}
	
}
