package com.crafting.kernel.domain.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.crafting.kernel.dao.PersonRepository;
import com.crafting.kernel.domains.Job;
import com.crafting.kernel.dto.JobDto;

@Component
@Scope("singleton")
public class JobMapper {

	@Autowired
	PersonRepository personRepo;
	
	public Job getJob(JobDto jobDto){
		Job job = new Job();
		job.setId(jobDto.getId());
		job.setFd(jobDto.getFd());
		job.setTitle(jobDto.getTitle());
		job.setSmallBody(jobDto.getSmallBody());
		job.setBody(jobDto.getBody());
		job.setCost(jobDto.getCost());
		job.setStatus(jobDto.getStatus());
		job.setAuthor(personRepo.findOne(jobDto.getAuthorId()));
		job.setPhotos(jobDto.getPhotos());
		job.setSkill(jobDto.getSkill());
		return job;
	}
	
	public JobDto getJobDto(Job job){
		JobDto jobDto = new JobDto();
		jobDto.setId(job.getId());
		jobDto.setFd(job.getFd());
		jobDto.setTd(job.getTd());
		jobDto.setTitle(job.getTitle());
		jobDto.setSmallBody(job.getSmallBody());
		jobDto.setBody(job.getBody());
		jobDto.setCost(job.getCost());
		jobDto.setStatus(job.getStatus());
		jobDto.setAuthorId(job.getAuthor().getId());
		jobDto.setPhotos(job.getPhotos());
		jobDto.setSkill(job.getSkill());
		return jobDto;
	}
	
}
