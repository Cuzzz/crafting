package com.crafting.kernel.domain.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.crafting.kernel.dao.PersonRepository;
import com.crafting.kernel.domains.Notification;
import com.crafting.kernel.dto.NotificationDto;

@Component
@Scope("singleton")
public class NotificationMapper {

	@Autowired
	PersonRepository personRepo;
	
	public Notification getNotification(NotificationDto noteDto){
		Notification note = new Notification();
		note.setId(noteDto.getId());
		note.setStatus(noteDto.getStatus());
		note.setType(noteDto.getType());
		note.setBody(noteDto.getBody());
		note.setDestination(personRepo.findOne(noteDto.getDestinationId()));
		return note;
	}
	
	public NotificationDto getNotificationDto(Notification note){
		NotificationDto noteDto = new NotificationDto();
		noteDto.setId(note.getId());
		noteDto.setDestinationId(note.getDestination().getId());
		noteDto.setStatus(note.getStatus());
		noteDto.setType(note.getType());
		noteDto.setBody(note.getBody());
		return noteDto;
	}
	
}
