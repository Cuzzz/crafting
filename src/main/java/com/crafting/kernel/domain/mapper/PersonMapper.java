package com.crafting.kernel.domain.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.crafting.kernel.domains.Person;
import com.crafting.kernel.dto.PersonDto;
import com.crafting.kernel.security.UserRepository;

@Component
@Scope("singleton")
public class PersonMapper {

	@Autowired
	UserRepository userRepo;
	
	public Person getPerson(PersonDto personDto){
		Person person = new Person();
		person.setFd(personDto.getFd());
		person.setGeneralRating(personDto.getGeneralRating());
		person.setId(personDto.getId());
		person.setRoleCode(personDto.getRoleCode());
		person.setStatus(personDto.getStatus());
		person.setTd(personDto.getTd());
		person.setUser(userRepo.findOne(personDto.getUserId()));
		person.setFirstname(personDto.getFirstName());
		person.setSurname(personDto.getSurname());
		person.setBirthDate(personDto.getBirthDate());
		person.setPhoneNumber(personDto.getPhoneNumber());
		person.setAvatarUrl(personDto.getAvatarUrl());
		person.setAbout(personDto.getAbout());
		person.setSkill(personDto.getSkill());
		return person;
	}
	
	public PersonDto getPersonDto(Person person){
		PersonDto personDto = new PersonDto();
		personDto.setFd(person.getFd());
		personDto.setGeneralRating(person.getGeneralRating());
		personDto.setId(person.getId());
		personDto.setRoleCode(person.getRoleCode());
		personDto.setStatus(person.getStatus());
		personDto.setTd(person.getTd());
		if(person.getUser() != null)
			personDto.setUserId(person.getUser().getId());
		else
			personDto.setUserId(0);
		personDto.setFirstName(person.getFirstname());
		personDto.setSurname(person.getSurname());
		personDto.setBirthDate(person.getBirthDate());
		personDto.setPhoneNumber(person.getPhoneNumber());
		personDto.setAvatarUrl(person.getAvatarUrl());
		personDto.setAbout(person.getAbout());
		personDto.setSkill(person.getSkill());
		return personDto;
	}
	
}
