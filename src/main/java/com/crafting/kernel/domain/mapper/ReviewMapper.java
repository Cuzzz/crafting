package com.crafting.kernel.domain.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.crafting.kernel.dao.JobRepository;
import com.crafting.kernel.dao.PersonRepository;
import com.crafting.kernel.domains.Review;
import com.crafting.kernel.dto.ReviewDto;

@Component
@Scope("singleton")
public class ReviewMapper {

	@Autowired
	JobRepository jobRepo;
	
	@Autowired
	PersonRepository personRepo;
	
	public Review getReview(ReviewDto reviewDto){
		Review review = new Review();
		review.setId(reviewDto.getId());
		review.setBody(reviewDto.getBody());
		review.setScore(reviewDto.getScore());
		review.setAuthor(personRepo.findOne(reviewDto.getAuthorId()));
		review.setSubject(personRepo.findOne(reviewDto.getSubjectId()));
		review.setJob(jobRepo.findOne(reviewDto.getJobId()));
		return review;
	}
	
	public ReviewDto getReviewDto(Review review){
		ReviewDto reviewDto = new ReviewDto();
		reviewDto.setId(review.getId());
		reviewDto.setBody(review.getBody());
		reviewDto.setScore(review.getScore());
		reviewDto.setAuthorId(review.getAuthor().getId());
		reviewDto.setSubjectId(review.getSubject().getId());
		reviewDto.setJobId(review.getJob().getId());
		return reviewDto;
	}
}
