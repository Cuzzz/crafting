package com.crafting.kernel.domains;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.crafting.kernel.security.User;

@Entity
@Table(name="person")
public class Person {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private long id;
	
	@Column(name = "fd")
	private Timestamp fd;
	
	@Column(name = "td")
	private Timestamp td;
	
	private String firstname;
	
	private String surname;
	
	private Date birthDate;
	
	private String phoneNumber;
	
	private String avatarUrl;
	
	private String about;
	
	@Column(name = "general_rate")
	private float generalRating;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "role_code")
	private byte roleCode;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="user_id") 
	@NotNull
	private User user;
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name = "person_skill", joinColumns = { 
			@JoinColumn(name = "id", nullable = false, updatable = false) }, 
			inverseJoinColumns = { @JoinColumn(name = "skill_id", nullable = false, updatable = false) })
	private Set<Skill> skill;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "author", fetch = FetchType.LAZY)
	private Set<Job> createdJobs;
	
	public Set<Skill> getSkill() {
		return skill;
	}

	public void setSkill(Set<Skill> skill) {
		this.skill = skill;
	}

	public Set<Job> getCreatedJobs() {
		return createdJobs;
	}

	public void setCreatedJobs(Set<Job> createdJobs) {
		this.createdJobs = createdJobs;
	}

	public long getId() {
		return id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Timestamp getFd() {
		return fd;
	}

	public void setFd(Timestamp fd) {
		this.fd = fd;
	}

	public Timestamp getTd() {
		return td;
	}

	public void setTd(Timestamp td) {
		this.td = td;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getAvatarUrl() {
		return avatarUrl;
	}

	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}

	public String getAbout() {
		return about;
	}

	public void setAbout(String about) {
		this.about = about;
	}
	
	public float getGeneralRating() {
		return generalRating;
	}

	public void setGeneralRating(float generalRating) {
		this.generalRating = generalRating;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public byte getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(byte roleCode) {
		this.roleCode = roleCode;
	}
}
