package com.crafting.kernel.dto;

import java.sql.Timestamp;

public class AcceptionDto {

	private long id;
	
	private Timestamp fd;
	
	private Timestamp td;
	
	private int status;

	private String body;
	
	private long authorId;
	
	private long jobId;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Timestamp getFd() {
		return fd;
	}

	public void setFd(Timestamp fd) {
		this.fd = fd;
	}

	public Timestamp getTd() {
		return td;
	}

	public void setTd(Timestamp td) {
		this.td = td;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(long authorId) {
		this.authorId = authorId;
	}

	public long getJobId() {
		return jobId;
	}

	public void setJobId(long jobId) {
		this.jobId = jobId;
	}
}
