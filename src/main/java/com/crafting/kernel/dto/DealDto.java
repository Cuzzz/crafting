package com.crafting.kernel.dto;

import java.sql.Timestamp;

public class DealDto {

	private long id;
	private Timestamp fd;
	private Timestamp td;
	private long jobId;
	private long customerId;
	private long employeeId;
	private int status;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Timestamp getFd() {
		return fd;
	}
	public void setFd(Timestamp fd) {
		this.fd = fd;
	}
	public Timestamp getTd() {
		return td;
	}
	public void setTd(Timestamp td) {
		this.td = td;
	}
	public long getJobId() {
		return jobId;
	}
	public void setJobId(long jobId) {
		this.jobId = jobId;
	}
	public long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}
	public long getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
}
