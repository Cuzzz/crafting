package com.crafting.kernel.dto;

import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

import com.crafting.kernel.domains.Skill;

public class JobDto {

	private long id;
	private Timestamp fd;
	private Timestamp td;
	private String title;
	private String smallBody;
	private String body;
	private float cost;
	private List<String> photos;
	private int status;
	private long authorId;
	private Set<Skill> skill;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Timestamp getFd() {
		return fd;
	}

	public void setFd(Timestamp fd) {
		this.fd = fd;
	}

	public Timestamp getTd() {
		return td;
	}

	public void setTd(Timestamp td) {
		this.td = td;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSmallBody() {
		return smallBody;
	}

	public void setSmallBody(String smallBody) {
		this.smallBody = smallBody;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public float getCost() {
		return cost;
	}

	public List<String> getPhotos() {
		return photos;
	}

	public void setPhotos(List<String> photos) {
		this.photos = photos;
	}

	public void setCost(float cost) {
		this.cost = cost;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(long authorId) {
		this.authorId = authorId;
	}

	public Set<Skill> getSkill() {
		return skill;
	}

	public void setSkill(Set<Skill> skill) {
		this.skill = skill;
	}
	
	
}
