package com.crafting.kernel.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class MessageDto {

	private long id;
	private String title;
	private String body;
	private int status;
	private long authorId;
	private long destinationId;
	private long upperMessageId;
	
	@JsonIgnore
	private boolean deletedForAuthor;
	
	@JsonIgnore
	private boolean deletedForDestination;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public long getAuthorId() {
		return authorId;
	}
	public void setAuthorId(long authorId) {
		this.authorId = authorId;
	}
	public long getDestinationId() {
		return destinationId;
	}
	public void setDestinationId(long destinationId) {
		this.destinationId = destinationId;
	}
	public long getUpperMessageId() {
		return upperMessageId;
	}
	public void setUpperMessageId(long upperMessageId) {
		this.upperMessageId = upperMessageId;
	}
	public boolean isDeletedForAuthor() {
		return deletedForAuthor;
	}
	public void setDeletedForAuthor(boolean deletedForAuthor) {
		this.deletedForAuthor = deletedForAuthor;
	}
	public boolean isDeletedForDestination() {
		return deletedForDestination;
	}
	public void setDeletedForDestination(boolean deletedForDestination) {
		this.deletedForDestination = deletedForDestination;
	}
}
