package com.crafting.kernel.enums;

public enum AcceptionStatus {

	ACTIVE(1),
	DECLINED(2),
	ACCEPTED(3);
	
	private int code;
	
	AcceptionStatus(int code){
		this.code = code;
	}
	
	public int getCode(){
		return this.code;
	}
}
