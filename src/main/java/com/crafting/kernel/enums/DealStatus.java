package com.crafting.kernel.enums;

public enum DealStatus {

	ACTIVE(1),
	DONE(2),
	INTERRUPTED(3);
	
	private int code;
	
	DealStatus(int code) {
		this.code = code;
	}
	
	public int getCode(){
		return code;
	}
	
}
