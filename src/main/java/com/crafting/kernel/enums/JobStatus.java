package com.crafting.kernel.enums;

public enum JobStatus {

	PREVIEW(1),
	ACTIVE(2),
	CLOSED(3),
	INWORK(4),
	DONE(5);
	
	private int code;
	
	JobStatus(int code) {
		this.code = code;
	}
	
	public int getCode(){
		return code;
	}
}
