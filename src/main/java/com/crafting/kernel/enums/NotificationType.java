package com.crafting.kernel.enums;

public enum NotificationType {

	ACCEPT (1),
	APPROVE (2),
	DECLINE (3), 
	DONE (4),
	MSG(5);
	
	private int code;
	
	NotificationType(int code) {
		this.code = code;
	}
	
	public int getCode(){
		return code;
	}
	
}
