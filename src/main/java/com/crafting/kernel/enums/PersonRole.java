package com.crafting.kernel.enums;

public enum PersonRole {
	
	CUSTOMER ((byte)1),
	EMPLOYEE ((byte)2);
	
	private byte code;
	
	PersonRole(byte code) {
		this.code = code;
	}
	
	public byte getCode(){
		return code;
	}
	
}