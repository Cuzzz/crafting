package com.crafting.kernel.runner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.crafting.kernel.security.User;
import com.crafting.kernel.security.UserRepository;
import com.crafting.kernel.security.UserRole;

@Configuration
@EnableAutoConfiguration
@EnableConfigurationProperties
@ComponentScan(basePackages={"com.crafting.kernel.security"
							, "com.crafting.kernel.config"
							, "com.crafting.kernel.web"
							, "com.crafting.kernel.service"
							, "com.crafting.kernel.domain.mapper"})
@EnableJpaRepositories(basePackages = {"com.crafting.kernel.security"
									  ,"com.crafting.kernel.dao"})
public class Runner
{
    public static void main( String[] args )
    {
    	ConfigurableApplicationContext ctx = SpringApplication.run(Runner.class, args);
    	UserRepository repo = ctx.getBean(UserRepository.class);
    	//addUser("admin", "admin", repo);
    	//addUser("user", "user", repo);
    }
    
	static private void addUser(String username, String password, UserRepository userRepository) {
		User user = new User();
		user.setUsername(username);
		user.setPassword(new BCryptPasswordEncoder().encode(password));
		user.grantRole(username.equals("admin") ? UserRole.ADMIN : UserRole.USER);
		userRepository.save(user);
	}
}
