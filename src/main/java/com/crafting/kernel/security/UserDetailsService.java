package com.crafting.kernel.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AccountStatusUserDetailsChecker;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

	@Autowired
	private UserRepository userRepo;

	private final AccountStatusUserDetailsChecker detailsChecker = new AccountStatusUserDetailsChecker();

	@Override
	public final User loadUserByUsername(String username) throws UsernameNotFoundException {
		final User user = userRepo.findByUsername(username);
		if (user == null) {
			throw new UsernameNotFoundException("user not found");
		}
		if (!user.getActive().equals("active"))
			throw new UsernameNotFoundException("Account was not activated");
		detailsChecker.check(user);
		return user;
	}
	
	public final User loadUserByEmail(String email){ 
		final User user = userRepo.findByEmail(email);
		if(user == null){
			throw new BadCredentialsException("Bad credentials");
		}
		if (!user.getActive().equals("active"))
			throw new UsernameNotFoundException("Account was not activated");
		detailsChecker.check(user);
		return user;
	}
}
