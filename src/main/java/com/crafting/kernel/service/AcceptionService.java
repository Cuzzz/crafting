package com.crafting.kernel.service;

import java.util.List;

import com.crafting.kernel.domains.Acception;
import com.crafting.kernel.domains.Job;
import com.crafting.kernel.domains.Person;

public interface AcceptionService {

	void save(Acception acception);
	void update(Acception acception) throws Exception;
	void delete(Acception acception);
	void delete(long id);
	Acception get(long id);
	List<Acception> getListByAuthor(Person author);
	List<Acception> getListByJob(Job job);
	
}
