package com.crafting.kernel.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crafting.kernel.dao.AcceptionRepository;
import com.crafting.kernel.domains.Acception;
import com.crafting.kernel.domains.Job;
import com.crafting.kernel.domains.Person;

@Service("acceptionService")
public class AcceptionServiceImpl implements AcceptionService {

	@Autowired
	AcceptionRepository acceptRepo;
	
	public void save(Acception acception) {
		acceptRepo.save(acception);
	}

	public void update(Acception acception) throws Exception {
		Acception savedAcception = acceptRepo.findOne(acception.getId());
		if(savedAcception == null)
			throw new Exception("Acception not found id="+ acception.getId());
		acception.setAuthor(savedAcception.getAuthor());
		acception.setJob(savedAcception.getJob());
		acception.setStatus(savedAcception.getStatus());
		acceptRepo.save(acception);
	}

	public void delete(Acception acception) {
		acceptRepo.delete(acception);
	}

	public Acception get(long id) {
		Acception acception = acceptRepo.findOne(id);
		return acception;
	}

	public List<Acception> getListByAuthor(Person author) {
		List<Acception> list = acceptRepo.findByAuthor(author);
		return list;
	}

	public List<Acception> getListByJob(Job job) {
		List<Acception> list = acceptRepo.findByJob(job);
		return list;
	}

	public void delete(long id) {
		acceptRepo.delete(id);
	}

}
