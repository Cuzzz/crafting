package com.crafting.kernel.service;

import java.util.List;

import com.crafting.kernel.domains.Deal;
import com.crafting.kernel.domains.Person;

public interface DealService {

	public Deal get(long id);
	public void save(Deal deal);
	public List<Deal> getListWhereEmployee(Person employee);
	public List<Deal> getListWhereCustomer(Person customer);
	public List<Deal> getListWhereJob(long jobId);
	public void saveAll(List<Deal> deals);
}
