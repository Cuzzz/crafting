package com.crafting.kernel.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crafting.kernel.dao.DealRepository;
import com.crafting.kernel.domains.Deal;
import com.crafting.kernel.domains.Job;
import com.crafting.kernel.domains.Person;

@Service("dealService")
public class DealServiceImpl implements DealService{

	@Autowired
	DealRepository dealRepo;
	
	@Autowired
	PersonService personService;
	
	@Autowired
	JobService jobService;
	
	public Deal get(long id) {
		Deal deal = dealRepo.getOne(id);
		return deal;
	}

	public void save(Deal deal) {
		dealRepo.saveAndFlush(deal);
	}
	
	public void saveAll(List<Deal> deals)
	{
		dealRepo.save(deals);
	}

	public List<Deal> getListWhereEmployee(Person employee) {
		List<Deal> list = dealRepo.findByEmployee(employee);
		return list;
	}

	public List<Deal> getListWhereCustomer(Person customer) {
		List<Deal> list = dealRepo.findByCustomer(customer);
		return list;
	}

	public List<Deal> getListWhereJob(long jobId) {
		Job job = jobService.get(jobId);
		List<Deal> list = dealRepo.findByJob(job);
		return list;
	}

}
