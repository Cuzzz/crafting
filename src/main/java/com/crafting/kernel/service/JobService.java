package com.crafting.kernel.service;

import java.util.List;

import com.crafting.kernel.domains.Job;
import com.crafting.kernel.domains.Person;

public interface JobService {

	public void save(Job job);
	public void update(Job job) throws Exception;
	public void delete(Job job);
	public void delete(long id);
	Job get(long id);
	List<Job> getList();
	public List<Job> getAuthorList(Person author);
	public List<Job> getPaginatedList(int page, int count);
}
