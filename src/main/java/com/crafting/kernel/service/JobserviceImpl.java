package com.crafting.kernel.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;

import com.crafting.kernel.dao.JobRepository;
import com.crafting.kernel.domains.Job;
import com.crafting.kernel.domains.Person;
import com.crafting.kernel.enums.JobStatus;

@Service("jobService")
public class JobserviceImpl implements JobService{

	@Autowired
	JobRepository jobRepo;
	
	@Autowired 
	PersonService personService;
	
	private Sort sort = new Sort(new Order(Sort.Direction.DESC, "id"));
	
	public void save(Job job) {
		jobRepo.save(job);
	}

	public void update(Job job) throws Exception {
		Job savedJob = jobRepo.findOne(job.getId());
		if(savedJob == null)
			throw new Exception("Job not found id="+job.getId());
		job.setAuthor(savedJob.getAuthor());
		job.setStatus(savedJob.getStatus());
		jobRepo.save(job);
	}

	public void delete(Job job) {
		jobRepo.delete(job);
	}

	public Job get(long id) {
		Job job = jobRepo.findOne(id);
		return job;
	}

	public List<Job> getList() {
		List<Job> list = jobRepo.findByStatus(JobStatus.ACTIVE.getCode(), sort);
		return list;
	}

	public void delete(long id) {
		jobRepo.delete(id);
	}
	
	public List<Job> getAuthorList(Person author){
		List<Job> list = jobRepo.findByAuthor(author);
		return list;
	}
	
	public List<Job> getPaginatedList(int page, int count){
		Pageable limit = new PageRequest(page, count, Sort.Direction.DESC, "id");
		Page<Job> result = jobRepo.findByStatus(JobStatus.ACTIVE.getCode(), limit);
		return result.getContent();
	}

}
