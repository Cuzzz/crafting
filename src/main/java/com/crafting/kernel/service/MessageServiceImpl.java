package com.crafting.kernel.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crafting.kernel.dao.MessageRepository;
import com.crafting.kernel.domains.Message;
import com.crafting.kernel.domains.Person;
import com.crafting.kernel.enums.MessageStatus;

@Service("msgService")
public class MessageServiceImpl implements MessageService{

	@Autowired
	MessageRepository msgRepository;
	
	public long save(Message msg) {
		msg = msgRepository.saveAndFlush(msg);
		return msg.getId();
	}

	public long read(Message msg) throws Exception {
		msg.setStatus(MessageStatus.READ.getCode());
		msg = msgRepository.saveAndFlush(msg);
		return msg.getId();
	}

	public long deleteForDestination(Message msg) {
		msg.setDeletedForDestination(true);
		msg = msgRepository.saveAndFlush(msg);
		return msg.getId();
	}

	public long deleteForAuthor(Message msg) {
		msg.setDeletedForAuthor(true);
		msg = msgRepository.saveAndFlush(msg);
		return msg.getId();
	}

	public List<Message> getByAuthor(Person author) {
		List<Message> list = msgRepository.findByAuthorAndDeletedForAuthor(author, false);
		return list;
	}

	public List<Message> getByDestination(Person dest) {
		List<Message> list = msgRepository.findByDestinationAndDeletedForDestination(dest, false);
		return list;
	}

	public Message findOne(long id) {
		return msgRepository.findOne(id);
	}
}
