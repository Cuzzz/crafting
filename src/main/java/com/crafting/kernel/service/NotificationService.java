package com.crafting.kernel.service;

import java.util.List;

import com.crafting.kernel.domains.Notification;
import com.crafting.kernel.domains.Person;

public interface NotificationService {

	void save(Notification note);
	void read(long noteId) throws Exception;
	List<Notification> getByPerson(Person person);
	
}
