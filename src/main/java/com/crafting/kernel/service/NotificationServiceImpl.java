package com.crafting.kernel.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crafting.kernel.dao.NotificationRepository;
import com.crafting.kernel.domains.Notification;
import com.crafting.kernel.domains.Person;
import com.crafting.kernel.enums.NotificationStatus;

@Service("notificationService")
public class NotificationServiceImpl implements NotificationService {

	@Autowired
	NotificationRepository noteRepo;
	
	public void save(Notification note) {
		noteRepo.save(note);
	}

	public void read(long noteId) throws Exception {
		Notification savedNote = noteRepo.findOne(noteId);
		if(savedNote == null)
			throw new Exception("Notification not found, id = " + noteId );
		savedNote.setStatus(NotificationStatus.READ.getCode());
		noteRepo.saveAndFlush(savedNote);
	}

	@Override
	public List<Notification> getByPerson(Person person) {
		List<Notification> list = noteRepo.findByDestinationAndStatus(person
											, NotificationStatus.NEW.getCode());
		return list;
	}

}
