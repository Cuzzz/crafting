package com.crafting.kernel.service;

import com.crafting.kernel.domains.Person;
import com.crafting.kernel.security.User;

public interface PersonService {

	void save(Person person);
	void update(Person person) throws Exception;
	void delete(Person person);
	Person get(long id);
	Person getByUser(User user);
	
}
