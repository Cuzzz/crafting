package com.crafting.kernel.service;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crafting.kernel.dao.PersonRepository;
import com.crafting.kernel.domains.Person;
import com.crafting.kernel.security.User;

@Service("personService")
public class PersonServiceImpl implements PersonService{

	@Autowired
	PersonRepository personRepo;
	
	
	public void save(Person person) {
		/*			MessageDigest md = MessageDigest.getInstance("MD5");
		byte[] hash = md.digest((person.getUser().getEmail()+person.getUser().getUsername()).getBytes("UTF-8"));
		person.getUser().setActive(hash.toString());*/
		person.getUser().setActive("active");
		personRepo.save(person);
	}

	public void update(Person person) throws Exception {
		Person savedPerson = personRepo.findOne(person.getId());
		if(savedPerson == null)
			throw new Exception("Cannot update person with id="+person.getId()+" , not found");
		person.setUser(savedPerson.getUser());
		person.setCreatedJobs(savedPerson.getCreatedJobs());
		personRepo.saveAndFlush(person);
	}

	public void delete(Person person) {
		personRepo.delete(person);
	}

	public Person get(long id) {
		return personRepo.findOne(id);
	}

	public Person getByUser(User user) {
		return personRepo.findByUser(user);
	}
}
