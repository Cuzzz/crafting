package com.crafting.kernel.service;

import java.util.List;

import com.crafting.kernel.domains.Person;
import com.crafting.kernel.domains.Review;

public interface ReviewService {

	void save(Review review);
	void update(Review review) throws Exception;
	Review get(long id);
	List<Review> getListBySubject(Person subject);
	List<Review> getListByAuthor(Person author);
	List<Review> getListByJob(long jobId);
	
}
