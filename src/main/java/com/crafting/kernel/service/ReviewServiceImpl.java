package com.crafting.kernel.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crafting.kernel.dao.ReviewRepository;
import com.crafting.kernel.domains.Job;
import com.crafting.kernel.domains.Person;
import com.crafting.kernel.domains.Review;

@Service("reviewService")
public class ReviewServiceImpl implements ReviewService {

	@Autowired
	ReviewRepository reviewRepo;
	
	@Autowired
	PersonService personService; 
	
	@Autowired
	JobService jobService;
	
	public void save(Review review) {
		reviewRepo.save(review);
	}

	public void update(Review review) throws Exception {
		Review savedReview = reviewRepo.findOne(review.getId());
		if(savedReview == null)
			throw new Exception("Review not found id="+review.getId());
		review.setAuthor(savedReview.getAuthor());
		review.setSubject(savedReview.getSubject());
		review.setJob(savedReview.getJob());
		reviewRepo.save(review);
	}

	public Review get(long id){
		return reviewRepo.findOne(id);
	}
	
	public List<Review> getListBySubject(Person subject) {
		List<Review> resultList = reviewRepo.findBySubject(subject);
		return resultList;
	}

	public List<Review> getListByAuthor(Person author) {
		List<Review> resultList = reviewRepo.findByAuthor(author);
		return resultList;
	}

	public List<Review> getListByJob(long jobId) {
		Job job  = jobService.get(jobId);
		List<Review> resultList = reviewRepo.findByJob(job);
		return resultList;
	}

}
