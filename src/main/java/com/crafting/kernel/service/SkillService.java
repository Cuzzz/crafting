package com.crafting.kernel.service;

import java.util.List;

import com.crafting.kernel.domains.Skill;

public interface SkillService {

	List<Skill> findAll();
	List<Skill> findAllByTitlePart(String titlePart);
	long save(Skill skill);
	Skill getByTitle(String title);
	Skill getById(long id);
	
}
