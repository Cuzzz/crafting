package com.crafting.kernel.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crafting.kernel.dao.SkillRepository;
import com.crafting.kernel.domains.Skill;

@Service("skillService")
public class SkillServiceImpl implements SkillService{

	@Autowired
	SkillRepository skillRepo;

	public List<Skill> findAll() {
		return skillRepo.findAll();
	}

	public List<Skill> findAllByTitlePart(String titlePart) {
		return skillRepo.findByTitleStartingWith(titlePart);
	}

	public long save(Skill skill) {
		skill = skillRepo.saveAndFlush(skill);
		return skill.getId();
	}

	public Skill getByTitle(String title) {
		return skillRepo.findByTitle(title);
	}

	public Skill getById(long id) {
		return skillRepo.findOne(id);
	}
	
}
