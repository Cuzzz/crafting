package com.crafting.kernel.web;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.crafting.kernel.commons.JsonResponse;
import com.crafting.kernel.domain.mapper.AcceptionMapper;
import com.crafting.kernel.domains.Acception;
import com.crafting.kernel.domains.Deal;
import com.crafting.kernel.domains.Job;
import com.crafting.kernel.domains.Notification;
import com.crafting.kernel.domains.Person;
import com.crafting.kernel.dto.AcceptionDto;
import com.crafting.kernel.enums.AcceptionStatus;
import com.crafting.kernel.enums.DealStatus;
import com.crafting.kernel.enums.JobStatus;
import com.crafting.kernel.enums.NotificationStatus;
import com.crafting.kernel.enums.NotificationType;
import com.crafting.kernel.enums.PersonRole;
import com.crafting.kernel.security.User;
import com.crafting.kernel.security.UserAuthentication;
import com.crafting.kernel.service.AcceptionService;
import com.crafting.kernel.service.DealService;
import com.crafting.kernel.service.JobService;
import com.crafting.kernel.service.NotificationService;
import com.crafting.kernel.service.PersonService;

@RestController
@RequestMapping("/acception")
public class AcceptionController {

	@Autowired
	private AcceptionService acceptService;
	
	@Autowired
	private AcceptionMapper acceptMapper;
	
	@Autowired
	private JobService jobService;
	
	@Autowired
	private PersonService personService;
	
	@Autowired
	private DealService dealService;
	
	@Autowired
	private NotificationService noteService;

	@RequestMapping(value = "/new", method = RequestMethod.PUT)
	public JsonResponse save(@RequestBody AcceptionDto acceptDto)
	{
		try{
			if(getCurrentUser().getRoleCode() != PersonRole.EMPLOYEE.getCode())
				throw new Exception("Only employee can add acceptions");
			Acception accept = acceptMapper.getAcception(acceptDto);
			accept.setAuthor(getCurrentUser());
			acceptService.save(accept);
			Notification note = new Notification();
			note.setDestination(accept.getJob().getAuthor());
			note.setStatus(NotificationStatus.NEW.getCode());
			note.setType(NotificationType.ACCEPT.getCode());
			note.setBody("job:"+accept.getJob().getId());
			noteService.save(note);
			return new JsonResponse(true);
		}
		catch(Exception e){
			return new JsonResponse(false, e.getMessage());
		}
	}
	
	@RequestMapping(value="/get/{id}", method = RequestMethod.GET)
	public JsonResponse getAcception(@PathVariable long id)
	{
		try{
			Acception accept = acceptService.get(id);
			AcceptionDto dto = acceptMapper.getAcceptionDto(accept);
			return new JsonResponse(true).setBody(dto);
		}
		catch(Exception e){
			return new JsonResponse(false, e.getMessage());
		}
	}
	
	@RequestMapping(value="/job/{id}", method=RequestMethod.GET)
	public JsonResponse getAcceptionsForJob(@PathVariable long id)
	{
		try{
			Job job = jobService.get(id);
			List<Acception> list = acceptService.getListByJob(job);
			if(list.size() == 0)
				throw new Exception("No acceptions found fo jobId = "+ id);
			List<AcceptionDto> resultList = new ArrayList<AcceptionDto>(list.size());
			for(Acception accept : list){
				resultList.add(acceptMapper.getAcceptionDto(accept));
			}
			return new JsonResponse(true).setBody(resultList);
		}
		catch(Exception e){
			return new JsonResponse(false, e.getMessage());
		}
	}
	
	@RequestMapping(value = "/author", method = RequestMethod.GET)
	public JsonResponse getAcceptionsForAuthor()
	{
		try{
			Person author = getCurrentUser();
			List<Acception> list = acceptService.getListByAuthor(author);
			if(list.size() == 0)
				throw new Exception("No acceptions found fo authorId = "+ author.getId());
			List<AcceptionDto> resultList = new ArrayList<AcceptionDto>(list.size());
			for(Acception accept : list){
				resultList.add(acceptMapper.getAcceptionDto(accept));
			}
			return new JsonResponse(true).setBody(resultList);
		}
		catch(Exception e){
			return new JsonResponse(false, e.getMessage());
		}
	}
	
	@RequestMapping(value = "/decline/{id}", method= RequestMethod.POST)
	public JsonResponse declineAcception(@PathVariable long id)
	{
		try{
			Acception accept = acceptService.get(id);
			if(!accept.getJob().getAuthor().equals(getCurrentUser()))
				throw new Exception("Detected trying decline acception not by job author");
			accept.setStatus(AcceptionStatus.DECLINED.getCode());
			acceptService.update(accept);
			Notification note = new Notification();
			note.setDestination(accept.getAuthor());
			note.setStatus(NotificationStatus.NEW.getCode());
			note.setType(NotificationType.DECLINE.getCode());
			note.setBody("job:"+accept.getJob().getId());
			noteService.save(note);
			return new JsonResponse(true);
		}
		catch(Exception e){
			return new JsonResponse(false, e.getMessage());
		}
		
	}
	
	@RequestMapping(value = "/approve/{id}", method = RequestMethod.POST)
	public JsonResponse approveAcception(@PathVariable long id)
	{
		try{
			Acception accept = acceptService.get(id);
			if(!accept.getJob().getAuthor().equals(getCurrentUser()))
				throw new Exception("Detected trying approve acception not by job author");
			accept.setStatus(AcceptionStatus.ACCEPTED.getCode());
			accept.getJob().setStatus(JobStatus.INWORK.getCode());
			Deal deal = new Deal();
			deal.setCustomer(getCurrentUser());
			deal.setEmployee(accept.getAuthor());
			deal.setJob(accept.getJob());
			deal.setStatus(DealStatus.ACTIVE.getCode());
			dealService.save(deal);
			acceptService.update(accept);
			Notification note = new Notification();
			note.setDestination(accept.getAuthor());
			note.setStatus(NotificationStatus.NEW.getCode());
			note.setType(NotificationType.APPROVE.getCode());
			note.setBody("job:"+accept.getJob().getId());
			noteService.save(note);
			return new JsonResponse(true);
		}
		catch(Exception e){
			return new JsonResponse(false, e.getMessage());
		}
	}
	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public JsonResponse deleteAcception(@PathVariable long id)
	{
		try{
			Acception accept = acceptService.get(id);
			if(!accept.getAuthor().equals(getCurrentUser()))
				throw new Exception("Detected trying of deleting acception not by author");
			acceptService.delete(id);
			return new JsonResponse(true);
		}
		catch(Exception e){
			return new JsonResponse(false, e.getMessage());
		}
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.PATCH)
	public JsonResponse updateAcception(@RequestBody AcceptionDto acceptDto)
	{
		try{
			Acception savedAcception = acceptService.get(acceptDto.getId());
			if(!savedAcception.getAuthor().equals(getCurrentUser()))
				throw new Exception("Detected trying updating acception not by job author");
			Acception acception = acceptMapper.getAcception(acceptDto);
			acceptService.update(acception);
			return new JsonResponse(true);
		}
		catch(Exception e){
			return new JsonResponse(false, e.getMessage());
		}
		
	}
	
	private Person getCurrentUser(){
		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		User user = null;
		if (authentication instanceof UserAuthentication) {
			 user = ((UserAuthentication) authentication).getDetails();
		}
		return personService.getByUser(user);
	}
	
}
