package com.crafting.kernel.web;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.crafting.kernel.commons.JsonResponse;
import com.crafting.kernel.domain.mapper.DealMapper;
import com.crafting.kernel.domains.Deal;
import com.crafting.kernel.domains.Job;
import com.crafting.kernel.domains.Notification;
import com.crafting.kernel.domains.Person;
import com.crafting.kernel.dto.DealDto;
import com.crafting.kernel.enums.DealStatus;
import com.crafting.kernel.enums.JobStatus;
import com.crafting.kernel.enums.NotificationStatus;
import com.crafting.kernel.enums.NotificationType;
import com.crafting.kernel.security.User;
import com.crafting.kernel.security.UserAuthentication;
import com.crafting.kernel.service.DealService;
import com.crafting.kernel.service.JobService;
import com.crafting.kernel.service.NotificationService;
import com.crafting.kernel.service.PersonService;

@RestController
@RequestMapping(value = "/deal")
public class DealController {

	@Autowired
	private DealService dealService;
	
	@Autowired
	private DealMapper dealMapper;
	
	@Autowired
	private JobService jobService;
	
	@Autowired
	private PersonService personService;
	
	@Autowired
	private NotificationService noteService;
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public JsonResponse getDeal(@PathVariable long id){
		try{
			Deal deal = dealService.get(id);
			if (deal == null)
				throw new Exception("Deal with id = "+id+" not found");
			DealDto dto = dealMapper.getDealDto(deal);
			return new JsonResponse(true).setBody(dto);
		}
		catch(Exception e){
			return new JsonResponse(false, e.getMessage());
		}
	}
	
	@RequestMapping(value = "/customer", method = RequestMethod.GET)
	public JsonResponse getDealsForCustomer(){
		try{
			Person current = getCurrentUser();
			List<Deal> list = dealService.getListWhereCustomer(current);
			if(list.isEmpty())
				throw new Exception("Deals for customer with id = "
									+current.getId()+" not found");
			List<DealDto> resultList = new ArrayList<DealDto>(list.size());
			for (Deal deal : list){
				resultList.add(dealMapper.getDealDto(deal));
			}
			return new JsonResponse(true).setBody(resultList);
		}
		catch(Exception e){
			return new JsonResponse(false, e.getMessage());
		}
	}
	
	@RequestMapping(value = "/employee", method = RequestMethod.GET)
	public JsonResponse getDealsForEmployee(){
		try{
			Person current = getCurrentUser();
			List<Deal> list = dealService.getListWhereEmployee(current);
			if(list.isEmpty())
				throw new Exception("Deals for employee with id = "
									+current.getId()+" not found");
			List<DealDto> resultList = new ArrayList<DealDto>(list.size());
			for (Deal deal : list){
				resultList.add(dealMapper.getDealDto(deal));
			}
			return new JsonResponse(true).setBody(resultList);
		}
		catch(Exception e){
			return new JsonResponse(false, e.getMessage());
		}
	}
	
	@RequestMapping(value = "/done/{id}", method = RequestMethod.POST)
	public JsonResponse doneSingleDeal(@PathVariable long id){
		try{
			Deal deal = dealService.get(id);
			if (deal == null)
				throw new Exception("Deal with id = "+id+" not found!");
			List<Deal> jobDealList = dealService.getListWhereJob(deal.getJob().getId());
			deal.setStatus(DealStatus.DONE.getCode());
			dealService.save(deal);
			if(jobDealList.size() == 1){
				Job job = deal.getJob();
				job.setStatus(JobStatus.DONE.getCode());
				jobService.update(job);
			}
			Notification note = new Notification();
			note.setDestination(deal.getEmployee());
			note.setStatus(NotificationStatus.NEW.getCode());
			note.setType(NotificationType.DONE.getCode());
			note.setBody("job:"+deal.getJob().getId());
			noteService.save(note);
			return new JsonResponse(true);
		}
		catch(Exception e){
			return new JsonResponse(false, e.getMessage());
		}
	}
	
	private Person getCurrentUser(){
		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		User user = null;
		if (authentication instanceof UserAuthentication) {
			 user = ((UserAuthentication) authentication).getDetails();
		}
		return personService.getByUser(user);
	}
}
