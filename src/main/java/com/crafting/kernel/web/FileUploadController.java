package com.crafting.kernel.web;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.crafting.kernel.commons.JsonResponse;

@RestController
public class FileUploadController {

	private RestTemplate restTemplate = new RestTemplate();
	
    @RequestMapping(value="/upload", method=RequestMethod.POST)
    public JsonResponse handleFileUpload(@RequestParam("file") MultipartFile file){
        if (!file.isEmpty()) {
            try {
                String fileType =  file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf('.'));
                File convFile = new File(DigestUtils.sha1Hex(file.getOriginalFilename())+fileType);
                convFile.createNewFile(); 
                FileOutputStream fos = new FileOutputStream(convFile); 
                fos.write(file.getBytes());
                fos.close(); 
                System.out.println(convFile);
                HttpHeaders authResult = authAtImageHosting();
                String storageUrl =  authResult.get("X-Storage-Url").get(0)+"craftbepublic/";
                HttpStatus status = sendFileToHosting(convFile, authResult.get("X-Storage-Url").get(0)+"craftbepublic/", authResult.get("X-Auth-Token").get(0));
                convFile.delete();
                if(status.compareTo(HttpStatus.CREATED) == 0)
                	return new JsonResponse(true).setBody(storageUrl+convFile.getName());
                throw new Exception("Failed");
            } catch (Exception e) {
                return new JsonResponse(false, e.getMessage());
            }
        } else {
        	return new JsonResponse(false, "Failed to upload because file was empty");
        }
    }
    
    private HttpStatus sendFileToHosting(File file, String baseUrl, String authToken) throws IOException{
    	String url = baseUrl+file.getName();
    	Resource resource = new FileSystemResource(file);
    	MultiValueMap<String, String> parts = new LinkedMultiValueMap<String, String>();
    	parts.add("X-Auth-Token", authToken);
    	HttpEntity<Object> entity = new HttpEntity<Object>(resource, parts) ;
    	ResponseEntity<String> result = restTemplate.exchange(url, HttpMethod.PUT, entity, String.class); 
    	return result.getStatusCode();
    }
    
    private HttpHeaders authAtImageHosting(){
    	String url = "https://auth.selcdn.ru/";
    	MultiValueMap<String, String> parts = new LinkedMultiValueMap<String, String>();
    	parts.add("X-Auth-User", "44732");
    	parts.add("X-Auth-Key", "MQTJEn2k");
    	ResponseEntity<String> result = restTemplate.exchange(url, HttpMethod.GET,  new HttpEntity<Object>(parts), String.class);
    	return result.getHeaders();
    }

}
