package com.crafting.kernel.web;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.crafting.kernel.commons.JsonResponse;
import com.crafting.kernel.domain.mapper.JobMapper;
import com.crafting.kernel.domains.Deal;
import com.crafting.kernel.domains.Job;
import com.crafting.kernel.domains.Notification;
import com.crafting.kernel.domains.Person;
import com.crafting.kernel.domains.Skill;
import com.crafting.kernel.dto.JobDto;
import com.crafting.kernel.enums.DealStatus;
import com.crafting.kernel.enums.JobStatus;
import com.crafting.kernel.enums.NotificationStatus;
import com.crafting.kernel.enums.NotificationType;
import com.crafting.kernel.enums.PersonRole;
import com.crafting.kernel.security.User;
import com.crafting.kernel.security.UserAuthentication;
import com.crafting.kernel.service.DealService;
import com.crafting.kernel.service.JobService;
import com.crafting.kernel.service.NotificationService;
import com.crafting.kernel.service.PersonService;
import com.crafting.kernel.service.SkillService;

@RestController
@RequestMapping("/job")
public class JobController {

	@Autowired
	private JobService jobService;
	
	@Autowired
	private JobMapper jobMapper;
	
	@Autowired
	private PersonService personService;
	
	@Autowired
	private DealService dealService;
	
	@Autowired
	private NotificationService noteService;
	
	@Autowired
	private SkillService skillService;
	
	@RequestMapping(value="/new", method = RequestMethod.PUT)
	public JsonResponse createJob(@RequestBody JobDto jobDto){
		try{
			if(getCurrentUser().getRoleCode() != PersonRole.CUSTOMER.getCode())
				throw new Exception("Only customers can add jobs");
			Job job = jobMapper.getJob(jobDto);
			job.setStatus(JobStatus.ACTIVE.getCode());
			job.setAuthor(getCurrentUser());
			Set<Skill> skills = job.getSkill();
			Set<Skill> newSkills = new HashSet<Skill>();
 			for(Skill skill : skills){
				Skill savedSkill = skillService.getByTitle(skill.getTitle());
				if(savedSkill != null)
					newSkills.add(savedSkill);
				else{
					long id = skillService.save(skill);
					newSkills.add(skillService.getById(id));
				}
			}
 			job.setSkill(newSkills);
			jobService.save(job);
			return new JsonResponse(true);
		}
		catch(Exception e){
			return new JsonResponse(false, e.getMessage());
		}
	}
	
	@RequestMapping(value="/close/{id}", method = RequestMethod.PATCH)
	public JsonResponse closeJob(@PathVariable long id){
		try{
			Job savedJob = jobService.get(id);
			if(!savedJob.getAuthor().equals(getCurrentUser()))
				throw new Exception("Detected trying of updating job not by author.");
			Job job = jobService.get(id);
			job.setStatus(JobStatus.CLOSED.getCode());
			jobService.save(job);
			return new JsonResponse(true);
		}
		catch(Exception e){
			return new JsonResponse(false, e.getMessage());
		}
	}
	
	@RequestMapping(value="/done/{id}", method = RequestMethod.PATCH)
	public JsonResponse finishJob(@PathVariable long id){
		try{
			Job savedJob = jobService.get(id);
			if(!savedJob.getAuthor().equals(getCurrentUser()))
				throw new Exception("Detected trying of updating job not by author.");
			Job job = jobService.get(id);
			
			job.setStatus(JobStatus.DONE.getCode());
			List<Deal> deals = dealService.getListWhereJob(job.getId());
			if(deals.isEmpty())
				throw new Exception("Cannot be done because no deals was created for job");
			for(Deal deal : deals){
				deal.setStatus(DealStatus.DONE.getCode());
				Notification note = new Notification();
				note.setDestination(deal.getEmployee());
				note.setStatus(NotificationStatus.NEW.getCode());
				note.setType(NotificationType.DONE.getCode());
				note.setBody("job:"+deal.getJob().getId());
				noteService.save(note);
			}
			dealService.saveAll(deals);
			jobService.save(job);
			return new JsonResponse(true);
		}
		catch(Exception e){
			return new JsonResponse(false, e.getMessage());
		}
	}
	
	@RequestMapping(value="/update", method = RequestMethod.PATCH)
	public JsonResponse updateJob(@RequestBody JobDto jobDto){
		try{
			Job savedJob = jobService.get(jobDto.getId());
			if(!savedJob.getAuthor().equals(getCurrentUser()))
				throw new Exception("Detected trying of updating job not by author.");
			Job job = jobMapper.getJob(jobDto);
			Set<Skill> skills = job.getSkill();
			Set<Skill> newSkills = new HashSet<Skill>();
 			for(Skill skill : skills){
				Skill savedSkill = skillService.getByTitle(skill.getTitle());
				if(savedSkill != null)
					newSkills.add(savedSkill);
				else{
					long id = skillService.save(skill);
					newSkills.add(skillService.getById(id));
				}
			}
 			job.setSkill(newSkills);
			jobService.update(job);
			return new JsonResponse(true);
		}
		catch(Exception e){
			return new JsonResponse(false, e.getMessage());
		}
	}
	
	@RequestMapping(value="/delete/{id}", method = RequestMethod.DELETE)
	public JsonResponse deleteJob(@PathVariable long id){
		try{
			Job savedJob = jobService.get(id);
			if(!savedJob.getAuthor().equals(getCurrentUser()))
				throw new Exception("Detected trying of updating job not by author.");
			jobService.delete(id);
			return new JsonResponse(true);
		}
		catch(Exception e){
			return new JsonResponse(false, e.getMessage());
		}
	}
	
	@RequestMapping(value="/get/{id}", method = RequestMethod.GET)
	public JsonResponse getJob(@PathVariable long id){
		try{
			Job job = jobService.get(id);
			JobDto dto = jobMapper.getJobDto(job);
			return new JsonResponse(true).setBody(dto);
		}
		catch(Exception e){
			return new JsonResponse(false, e.getMessage());
		}
	}
	
	@RequestMapping(value="/author", method = RequestMethod.GET)
	public JsonResponse getJobListOfAuthor(){
		try{
			Person current = getCurrentUser();
			List<Job> list = jobService.getAuthorList(current);
			if(list.size() == 0)
				throw new Exception("No jobs found for authorId = " + current.getId());
			List<JobDto> resultList = new ArrayList<JobDto>(list.size());
			for(Job job : list){
				resultList.add(jobMapper.getJobDto(job));
			}
			return new JsonResponse(true).setBody(resultList);
		}
		catch(Exception e){
			return new JsonResponse(false, e.getMessage());
		}
	}
	
	
	@RequestMapping(value="/list", method = RequestMethod.GET)
	public JsonResponse getJobList(@RequestParam(required=false, defaultValue = "0") int page
								 , @RequestParam(required=false, defaultValue = "0") int count){
		try{
			List<Job> list = new ArrayList<Job>();
			if(count ==0){
				list = jobService.getList();
			}
			else{
				list = jobService.getPaginatedList(page, count);
			}
			List<JobDto> resultList = new ArrayList<JobDto>(list.size()); 
			for(Job job : list){
				resultList.add(jobMapper.getJobDto(job));
			}
			return new JsonResponse(true).setBody(resultList);
		}
		catch(Exception e){
			return new JsonResponse(false, e.getMessage());
		}
	}
	
	private Person getCurrentUser(){
		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		User user = null;
		if (authentication instanceof UserAuthentication) {
			 user = ((UserAuthentication) authentication).getDetails();
		}
		return personService.getByUser(user);
	}
}