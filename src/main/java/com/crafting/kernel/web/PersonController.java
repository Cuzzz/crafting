package com.crafting.kernel.web;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.crafting.kernel.commons.JsonResponse;
import com.crafting.kernel.domain.mapper.PersonMapper;
import com.crafting.kernel.domains.Person;
import com.crafting.kernel.domains.Skill;
import com.crafting.kernel.dto.PersonDto;
import com.crafting.kernel.security.User;
import com.crafting.kernel.security.UserAuthentication;
import com.crafting.kernel.security.UserRepository;
import com.crafting.kernel.security.UserRole;
import com.crafting.kernel.service.PersonService;
import com.crafting.kernel.service.SkillService;

/** Example controller for checking right configuration of hibernate
 * for checking saving/getting users just request to 
 * localhost:8080/save 
 * better if you repeat this by some times
 * and then you can call? for example
 * localhost:8080/find/2 and get json response with person info
 */
@RestController
public class PersonController {
	
	@Autowired
	PersonService personService;
	
	@Autowired
	UserRepository userRepo;
	
	@Autowired
	PersonMapper personMapper;
	
	@Autowired
	SkillService skillService;
	
	private final String REG_MSG = "You have registered at craftbe.org. For activating your "
			+ "account please follow <a href=\"http://localhost:8080/person/activate/%s\">link</a>";
	
	@RequestMapping(value = "/person/register", method = RequestMethod.POST)
	public JsonResponse register(@RequestBody Person person){
		try{
			User user= person.getUser();
			user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
			user.grantRole(UserRole.USER);
			Set<Skill> skills = person.getSkill();
			Set<Skill> newSkills = new HashSet<Skill>();
 			for(Skill skill : skills){
				Skill savedSkill = skillService.getByTitle(skill.getTitle());
				if(savedSkill != null)
					newSkills.add(savedSkill);
				else{
					long id = skillService.save(skill);
					newSkills.add(skillService.getById(id));
				}
			}
 			person.setSkill(newSkills);
			userRepo.save(user);
			personService.save(person);
			//MailSender.sendMail("Registation at craftbe.org", String.format(REG_MSG, user.getActive()), user.getEmail());
			return new JsonResponse(true);
		}
		catch(Exception e){
			return new JsonResponse(false, e.getMessage());
		}
	}
	
	@RequestMapping(value = "/person/find/{id}", method = RequestMethod.GET)
	public JsonResponse getPersonInfo(@PathVariable long id){
		Person person = personService.get(id);
		if(person == null)
			return new JsonResponse(false, "person not found");
		PersonDto personDto = personMapper.getPersonDto(person);
		return new JsonResponse(true).setBody(personDto);
	}
	
	@RequestMapping(value = "/person/update", method = RequestMethod.PATCH)
	public JsonResponse updatePersonInfo(@RequestBody PersonDto personDto){
		try{
			Person savedPerson = personService.get(personDto.getId());
			if(!savedPerson.equals(getCurrentUser()))
				throw new Exception("Not owner trying to update account");
			Person person = personMapper.getPerson(personDto);
			Set<Skill> skills = person.getSkill();
			Set<Skill> newSkills = new HashSet<Skill>();
 			for(Skill skill : skills){
				Skill savedSkill = skillService.getByTitle(skill.getTitle());
				if(savedSkill != null)
					newSkills.add(savedSkill);
				else{
					long id = skillService.save(skill);
					newSkills.add(skillService.getById(id));
				}
			}
 			person.setSkill(newSkills);
			personService.update(person);
			return new JsonResponse(true);
		}
		catch(Exception e){
			return new JsonResponse(false, e.getMessage());
		}
	}
	
	@RequestMapping(value = "/person/activate/{code}", method = RequestMethod.GET)
	public JsonResponse activatePerson(@PathVariable String code){
		User user = userRepo.findByActive(code);
		if (user == null){
			return new JsonResponse(false, "User not found or already active");
		}
		user.setActive("active");
		userRepo.save(user);
		return new JsonResponse(true);
	}
	
	@RequestMapping(value = "/person/check", method = RequestMethod.GET)
	public JsonResponse checkByEmail(@RequestParam("email") String email){
		User user  = userRepo.findByEmail(email);
		if (user == null) {
			return new JsonResponse(true,"Email s not busy");
		}
		return new JsonResponse(false,"Email is busy try other");
		
	}
	
	private Person getCurrentUser(){
		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		User user = null;
		if (authentication instanceof UserAuthentication) {
			 user = ((UserAuthentication) authentication).getDetails();
		}
		return personService.getByUser(user);
	}
	

}
