package com.crafting.kernel.web;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.crafting.kernel.commons.JsonResponse;
import com.crafting.kernel.domain.mapper.ReviewMapper;
import com.crafting.kernel.domains.Deal;
import com.crafting.kernel.domains.Person;
import com.crafting.kernel.domains.Review;
import com.crafting.kernel.dto.ReviewDto;
import com.crafting.kernel.enums.DealStatus;
import com.crafting.kernel.security.User;
import com.crafting.kernel.security.UserAuthentication;
import com.crafting.kernel.service.DealService;
import com.crafting.kernel.service.PersonService;
import com.crafting.kernel.service.ReviewService;

@RestController
@RequestMapping("/review")
public class ReviewController {
	
	@Autowired
	ReviewMapper reviewMapper;
	
	@Autowired
	ReviewService reviewService;
	
	@Autowired
	PersonService personService;
	
	@Autowired
	DealService dealService;

	@RequestMapping(value = "/new", method = RequestMethod.PUT)
	public JsonResponse createReview(@RequestBody ReviewDto reviewDto){
		try{
			List<Deal> deals = dealService.getListWhereJob(reviewDto.getJobId());
			if(deals.size() == 0)
				throw new Exception("No deals found for job = "+reviewDto.getJobId());
			byte role = getCurrentUser().getRoleCode();
			boolean finishedDeal = false;
			for (Deal deal : deals){
				if(role == 1){
					if(deal.getCustomer().equals(getCurrentUser()) 
							&& deal.getStatus() == DealStatus.DONE.getCode()){
						finishedDeal= true;
						break;
					}
				}
				if(role == 2){
					if(deal.getEmployee().equals(getCurrentUser()) 
							&& deal.getStatus() == DealStatus.DONE.getCode()){
						finishedDeal= true;
						break;
					}
				}
			}
			if(!finishedDeal)
				throw new Exception("Cannot create review for job  = "+ reviewDto.getJobId()
									+ " , author = " + reviewDto.getAuthorId() 
									+ " , subject = " + reviewDto.getSubjectId());
			Review review = reviewMapper.getReview(reviewDto);
			review.setAuthor(getCurrentUser());
			reviewService.save(review);
			List<Review> list = reviewService.getListBySubject(review.getSubject());
			float total = 0.0f;
			for(Review item : list ){
				total += item.getScore();
			}
			Person subject = review.getSubject();
			subject.setGeneralRating(total/list.size());
			personService.update(subject);
			return new JsonResponse(true);
		}
		catch(Exception e){
			return new JsonResponse(false, e.getMessage());
		}
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.PATCH)
	public JsonResponse updateReview(@RequestBody ReviewDto reviewDto){
		try{
			Review savedReview = reviewService.get(reviewDto.getId());
			if(!savedReview.getAuthor().equals(getCurrentUser()))
				throw new Exception("Detected trying of updating review not by author.");
			Review review = reviewMapper.getReview(reviewDto);
			reviewService.update(review);
			return new JsonResponse(true);
		}
		catch(Exception e){
			return new JsonResponse(false, e.getMessage());
		}
	}
	
	@RequestMapping(value = "/author", method = RequestMethod.GET)
	public JsonResponse getByAuthor(){
		try{
			Person current = getCurrentUser();
			List<Review> list = reviewService.getListByAuthor(current);
			if(list.size() == 0)
				throw new Exception("No reviews found for authorId = " + current.getId());
			List<ReviewDto> resultList = new ArrayList<ReviewDto>(list.size());
			for(Review review : list){
				resultList.add(reviewMapper.getReviewDto(review));
			}
			return new JsonResponse(true).setBody(resultList);
		}
		catch(Exception e){
			return new JsonResponse(false, e.getMessage());
		}
	}
	
	@RequestMapping(value = "/subject/{id}", method = RequestMethod.GET)
	public JsonResponse getBySubject(@PathVariable long id){
		try{
			Person current = getCurrentUser();
			List<Review> list = reviewService.getListBySubject(current);
			if(list.size() == 0)
				throw new Exception("No reviews found for subjectId = " + current.getId());
			List<ReviewDto> resultList = new ArrayList<ReviewDto>(list.size());
			for(Review review : list){
				resultList.add(reviewMapper.getReviewDto(review));
			}
			return new JsonResponse(true).setBody(resultList);
		}
		catch(Exception e){
			return new JsonResponse(false, e.getMessage());
		}
	}
	
	@RequestMapping(value = "/job/{id}", method = RequestMethod.GET)
	public JsonResponse getByJob(@PathVariable long id){
		try{
			List<Review> list = reviewService.getListByJob(id);
			if(list.size() == 0)
				throw new Exception("No reviews found for jobId = " + id);
			List<ReviewDto> resultList = new ArrayList<ReviewDto>(list.size());
			for(Review review : list){
				resultList.add(reviewMapper.getReviewDto(review));
			}
			return new JsonResponse(true).setBody(resultList);
		}
		catch(Exception e){
			return new JsonResponse(false, e.getMessage());
		}
	}
	
	private Person getCurrentUser(){
		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		User user = null;
		if (authentication instanceof UserAuthentication) {
			 user = ((UserAuthentication) authentication).getDetails();
		}
		return personService.getByUser(user);
	}
	
}
