package com.crafting.kernel.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.crafting.kernel.commons.JsonResponse;
import com.crafting.kernel.domains.Skill;
import com.crafting.kernel.service.SkillService;

@RestController
@RequestMapping("/skill")
public class SkillController {

	@Autowired
	SkillService skillService;
	
	@RequestMapping(value = "/new", method = RequestMethod.PUT)
	public JsonResponse save(@RequestBody Skill skill){
		try{
			long id = skillService.save(skill);
			return new JsonResponse(true).setBody(id);
		}
		catch(Exception e){
			return new JsonResponse(false, e.getMessage());
		}
	}
	
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public JsonResponse getAll(){
		try{
			List<Skill> list = skillService.findAll();
			return new JsonResponse(true).setBody(list);
		}
		catch(Exception e){
			return new JsonResponse(false, e.getMessage());
		}
	}
	
	@RequestMapping(value = "/find", method = RequestMethod.GET)
	public JsonResponse getByName(@RequestParam(required=false, defaultValue = "") String title){
		try{
			List<Skill> list = skillService.findAllByTitlePart(title);
			return new JsonResponse(true).setBody(list);
		}
		catch(Exception e){
			return new JsonResponse(false, e.getMessage());
		}
	}
	
}
